import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class QuizQuestion{
  question: string;
  options: string[];
  correctOption:number;

  constructor() { }
}
