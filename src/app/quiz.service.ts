import { Injectable } from '@angular/core';
import { QuizQuestion } from '../question.service';




@Injectable({
  providedIn: 'root'
})


export class QuizService {
  private questions: QuizQuestion[] = [];
  private activeQuestion: QuizQuestion;
  private isCorrect: boolean;
  private isSelected: boolean;
  private questionCounter: number;
  private optionCounter: number;
  private startTime: Date;
  private endTime: Date;
  private duration: number;
  private numberOfCorrect: number;

  constructor() { 
  }
  public addQuestions() {
      this.questions = [{
        question: 'good',
        options:['过',
      '吗',
    '好'],
    correctOption: 2
 },
   {
    question: 'person',
      options:[
         '人',
        '中',
        '你',
         '我'],
   correctOption: 0
  },
   {
    question: 'I',
     options: [
      '他',
      '我',
      '是',
    '或',
 '日'],
     correctOption: 1
              },
              {
      question: 'finland',
        options: [
     '中国',
     '没够',
    '泰国',
     '韩国',
     '芬兰',
     '瑞典'],
     correctOption: 4
          }
         ];
      }
 
           

public setQuostion() {
  this.optionCounter=0;
  this.isCorrect = false;
  this.isSelected = false;
  this.activeQuestion = this.questions[this.questionCounter];
  this.questionCounter++;
}

public initialize () {
  this.questionCounter=0;
  this.startTime = new Date();
  this.setQuostion();
}
public getQuostions (): QuizQuestion[] {
  return this.questions;
}
public getActiveQuestion(): QuizQuestion {
  return this.activeQuestion;
}

public getQuestionOfActiveQuestion(): string {
  return this.activeQuestion.question;
}

public getOptionsOfActiveQuestion(): string [] {
  return this.activeQuestion.options;
}
public getIndexOfActiveQuestion(): number {
  return this.questionCounter;
}

public getNumberOfQuestions(): number {
  return this.questions.length;
}
public getNumberOfQuestionOfActiveQuestion(): number {
return this.activeQuestion.options.length

}

public getIndexOptionCounter() {
  return this.optionCounter;
}
public getCorrectOptionOfActiveQuestion(): string {
  return this.activeQuestion.options[this.activeQuestion.correctOption];
}
public setOptionSelected() {
  this.isSelected = true;
}
public isOptionSelected(): boolean {
  return this.isSelected;
}
public areAllOptionsUsed(): boolean {
  this.optionCounter++;
  return (this.optionCounter > this.activeQuestion.options.length)
  ? true : false;
}
public isCorrectOption(option: number) {
  this.isCorrect=
  (option === this.activeQuestion.correctOption) ? true : false;
  return this.isCorrect;
}
public isAnswerCorrect(): boolean {
  return this.isCorrect;
}
public isFinished(): boolean {
  return (this.questionCounter === this.questions.length)
  ? true : false;
}
public getDuration(): number {
  this.endTime = new Date();
  this.duration = this.endTime.getTime() - this.startTime.getTime();
  return this.duration;
}
public addNumberOfCorrect() {
      this.numberOfCorrect++;
}
public getNumberOfCorrect(): number {
  return this.numberOfCorrect;
} 

}
