import { NumberSymbol } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { QuizService } from '../quiz.service';
 
@Component({
  selector: 'app-result',
  templateUrl: './result.page.html',
  styleUrls: ['./result.page.scss'],
})
export class ResultPage implements OnInit {

constructor(public router: Router,
public activatedRoute: ActivatedRoute,
private quizService: QuizService) { }
private duration: number;
private durationSeconds: number;

  ngOnInit() {
    this.duration = Number(this.activatedRoute.snapshot.paramMap.get('duration'));
    this.durationSeconds = Math.round((this.duration)/ 1000);
  }
  private goAnswers() {
    this.quizService.initialize();
    this.router.navigateByUrl('answers');
  }

}
