import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { QuizService } from '../quiz.service';

@Component({
  selector: 'app-start',
  templateUrl: './start.page.html',
  styleUrls: ['./start.page.scss'],
})
export class StartPage implements OnInit {

  constructor(public router: Router,
    public activatedRoute: ActivatedRoute,
    private quizService: QuizService)  { }

  ngOnInit() {
  }



     private goHome() {
      this.quizService.initialize();
      this.router.navigateByUrl('home');
   }

  };