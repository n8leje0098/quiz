import { Component } from '@angular/core';
import { QuizService } from '../quiz.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  constructor(public router: Router, private quizService: QuizService) {}

  private feedback: string;
  private duration: number;

  ngOnInit() {
    this.quizService.addQuestions();
    this.quizService.initialize();
    this.feedback = "";
  }
  private checkOption(option: number) {
    this.quizService.setOptionSelected();
    if (this.quizService.areAllOptionsUsed() ) {
      this.quizService.setQuostion();
  }
  else {
    if (this.quizService.isCorrectOption(option)) {
      this.quizService.addNumberOfCorrect();
      this.quizService.getCorrectOptionOfActiveQuestion();
      this.feedback = 'correct, continiue'
    }
    else {
      this.quizService.getCorrectOptionOfActiveQuestion() 
      this.feedback = 'incorrect, continue ';
  
  }
  }
}

private continue () {
  if (this.quizService.isFinished()) {
    this.duration = this.quizService.getDuration();
    this.router.navigateByUrl('result/' + this.duration);
  }
  else {
    this.quizService.setQuostion();
  }
}


}


